package AsteroidFieldExpander;

import api.listener.Listener;
import api.listener.events.world.*;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.FileConfiguration;
import api.utils.game.chat.CommandInterface;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.server.ServerMessage;

import javax.annotation.Nullable;
import javax.vecmath.Vector3f;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

//Praise be to the Machine God!
//01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110 00100000 01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110 00100000 01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110

public class Main extends StarMod {
    private Random rng = new Random();
    private ArrayList<Vector3i> positionsUsed = new ArrayList<>();

    //TODO: Configs!

    /**Hard cap on how many rocks can exist in a sector*/
    public static int rocksCap;

    /**Space (in blocks) between asteroid spawn points.*/
    public static int gridUnitSize;

    /** How far an asteroid can be offset from its grid node.*/
    public static int maxOffset;

    /** How much, at maximum, to fill up the sector with asteroids. 1 = every grid node could get a rock (a.k.a. lag)*/
    public static float maxFill;

    /** Minimum rock density as a fraction of maximum defined by FillFactor*/
    public static float minFill;

    /** CONFIGURED: Fraction of sector size that is used as margin (prevents sector border-hugging asteroids and associated screwiness)*/
    public static float boundFactor;

    /** CALCULATED: Length of boundary space between furthest allowable asteroid spawn node and edge of sector*/
    private static int boundMargin;

    /** Highest possible coordinate in maximum asteroid widths*/
    public static int maxNodeDim;

    public static int sectorScale;

    public static void main(String[] args) {
        //just to make the env happy lol
    }

    /**
     * Returns the real-space, sector-scale coordinates of an asteroid at a given internal grid position.
     */
    private Vector3f getActualPosShift(Vector3i gridpos) {
        Vector3f result = new Vector3f();
        Vector3f gridshift = new Vector3f();
        Vector3f offset = new Vector3f();

        //corner-based coordinates
        gridshift.x = boundMargin + (gridpos.x * gridUnitSize);
        gridshift.y = boundMargin + (gridpos.y * gridUnitSize);
        gridshift.z = boundMargin + (gridpos.z * gridUnitSize);

        //random offset to to make it naturalistic
        offset.x = nextGridOffset();
        offset.y = nextGridOffset();
        offset.z = nextGridOffset();

        float h = -0.5f * sectorScale; //Half the sector's size to subtract, to get center-based coordinates
        result.add(new Vector3f(h,h,h));

        result.add(gridshift);
        result.add(offset);

        return result;
    }

    @Override
    public void onEnable() {
        StarLoader.registerListener(AsteroidNormalPopulateEvent.class, new Listener<AsteroidNormalPopulateEvent>() {
            @Override
            public void onEvent(AsteroidNormalPopulateEvent asteroidNormalPopulateEvent) {
                try {
                    updateConfiguration(true);

                    updateSectorScale();
                    //True offset for the grid dimensions must allow for both the 'under' and 'above' bound margins,
                    //as well as potential asteroid offset.
                    positionsUsed.clear(); //clear old occupied zone cache

                    if (asteroidNormalPopulateEvent.getSector().getSectorType() == SectorInformation.SectorType.ASTEROID) {
                        int volume = (int) FastMath.pow(maxNodeDim, 3); //maximum physically allowable amount of spawn nodes for asteroids

                        int maxRocks = Math.min((int)(volume * maxFill), rocksCap); //how many rocks allowed to spawn here
                        int minRocks = Math.min((int)(volume * minFill), rocksCap);

                        asteroidNormalPopulateEvent.setMinAsteroidPopulation(minRocks);
                        asteroidNormalPopulateEvent.setMaxAsteroidPopulation(maxRocks);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, this);

        StarLoader.registerListener(AsteroidPositionEvent.class, new Listener<AsteroidPositionEvent>() {
            @Override
            public void onEvent(AsteroidPositionEvent asteroidPositionEvent) {
                asteroidPositionEvent.forceMarkPosChanged(); //prevent StarMade from killing my asteroids :(
                FloatingRock chonk = asteroidPositionEvent.getAsteroid();
                Vector3i gridpos = new Vector3i();
                boundMargin = (int) (sectorScale * boundFactor);
                int gridMargin = 2 * boundMargin + 2 * maxOffset;
                maxNodeDim = (int)((sectorScale-gridMargin)/gridUnitSize); //highest possible node is (maxNodeDim,maxNodeDim,maxNodeDim)

                do {
                    gridpos.x = rng.nextInt(maxNodeDim);
                    gridpos.y = rng.nextInt(maxNodeDim);
                    gridpos.z = rng.nextInt(maxNodeDim);
                    //System.err.println("[MOD][AsteroidFieldExpander] Trying grid node position " + gridpos.toString() + ".");
                }
                while (positionsUsed.contains(gridpos));
                //System.err.println("[MOD]AsteroidFieldExpander] Using grid node position "+gridpos.toString()+" for rock.");

                positionsUsed.add(gridpos);

                Vector3f shift = getActualPosShift(gridpos);
                float x = shift.x;
                float y = shift.y;
                float z = shift.z;

                System.err.println("[MOD][AsteroidFieldExpander] Calculated target position for asteroid at (" + x + ", " + y + ", " + z + ").");

                //System.err.println("[MOD][AsteroidFieldExpander] Current initial transform: " + chonk.getInitialTransform().origin.toString());
                //System.err.println("[MOD]AsteroidFieldExpander] Current world transform: " + chonk.getWorldTransform().origin.toString());
                //System.err.println("[MOD][AsteroidFieldExpander] Forcing change to transforms.");
                chonk.getInitialTransform().origin.set(new Vector3f(x, y, z));
                chonk.getWorldTransform().origin.set(new Vector3f(x, y, z));
                //System.err.println("[MOD][AsteroidFieldExpander] Current initial transform is finally: " + chonk.getInitialTransform().origin.toString());
                //System.err.println("[MOD][AsteroidFieldExpander] Current world transform is finally: " + chonk.getWorldTransform().origin.toString());
            }
        }, this);
        StarLoader.registerListener(AsteroidPreSpawnEvent.class, new Listener<AsteroidPreSpawnEvent>(){
            @Override
            public void onEvent(AsteroidPreSpawnEvent asteroidPreSpawnEvent){
                FloatingRock chonk = asteroidPreSpawnEvent.getAsteroid();
                int sizeMin = Sector.rockSize; //Seems like when rocks go below this size, they have a bad habit of being blockless segmentcontrollers. No thanks!
                int sizeMax = (Integer)ServerConfig.ASTEROID_RADIUS_MAX.getCurrentState();
                Vector3i size = new Vector3i(sizeMin,sizeMin,sizeMin);
                size.x = Math.max(sizeMin, sizeMin + (int)(nextAsteroidSizeFactor() * Math.abs(sizeMax-sizeMin)));
                size.y = Math.max(sizeMin, sizeMin + (int)(nextAsteroidSizeFactor() * Math.abs(sizeMax-sizeMin)));
                size.z = Math.max(sizeMin, sizeMin + (int)(nextAsteroidSizeFactor() * Math.abs(sizeMax-sizeMin)));
                //System.err.println("[MOD][AsteroidFieldExpander] Asteroid size selected: " + size.toString());
                chonk.loadedGenSize = size;
            }
        }, this);

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "set_asteroid_cap";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState playerState, String[] strings) {
                if(strings.length == 1) {
                    try {
                        int val = Integer.parseInt(strings[0]);
                        if(val == 0) {
                            playerState.sendServerMessage(new ServerMessage(new Object[]{Lng.str("ERROR: Number must be an integer greater than 0")}, ServerMessage.MESSAGE_TYPE_SIMPLE));
                            return false;
                        }
                        rocksCap = val;
                        FileConfiguration config = getConfig("AsteroidFieldExpander");
                        config.set("AsteroidSectorPopulationCap",val);
                        updateConfiguration(true);
                        return true;
                    }
                    catch (NumberFormatException nx){
                        return false;
                    }
                }
                else return false;
            }

            @Override @Deprecated
            public void serverAction(@Nullable PlayerState playerState, String[] strings) {}

            @Override
            public StarMod getMod() {
                return null;
            }
        });

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "refresh_config";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState playerState, String[] strings) {
                if(strings.length == 0) {
                    updateConfiguration(false);
                    return true;
                }
                else return false;
            }

            @Override @Deprecated
            public void serverAction(@Nullable PlayerState playerState, String[] strings) {}

            @Override
            public StarMod getMod() {
                return null;
            }
        });
    }

    /**
     * Updates the mod's internal configuration state.
     * @param writeToFile Whether or not to create a config or write default values if the config or value entries don't currently exist.
     */
    public void updateConfiguration(boolean writeToFile){
        FileConfiguration config = getConfig("AsteroidFieldExpander");

        rocksCap = Integer.parseInt(config.getConfigurableValue("AsteroidSectorPopulationCap","15"));
        gridUnitSize = Integer.parseInt(config.getConfigurableValue("AsteroidSpawningCellWidth", "1500"));
        maxOffset = (gridUnitSize/2) - ((Integer)ServerConfig.ASTEROID_RADIUS_MAX.getCurrentState() * 4); //half a grid box minus two max asteroid widths should mean no collisions ever
        maxFill = Float.parseFloat(config.getConfigurableValue("AsteroidFillPercentageMax", "0.1")); //irrelevant tbh; the game can't neatly handle filling any reasonable percentage of the grid squares
        minFill = Float.parseFloat(config.getConfigurableValue("AsteroidFillPercentageMin","0.05"));
        boundFactor = Float.parseFloat(config.getConfigurableValue("AsteroidSpawningBoundaryFactor", "0.03"));

        if(writeToFile) config.saveConfig();
    }

    public float nextSignedFloat() {
        return (rng.nextFloat() * 2) - 1;
    }

    private float nextGridOffset(){
        return 2 * nextSignedFloat() * maxOffset; //zero-centered offset
    }

    /**
     * The method used to determine what size between minimum and maximum each asteroid should use.
     * Should always return between zero and one.
     * @return randomized number between 0.0f and 1.0f based on some distribution
     */
    public float nextAsteroidSizeFactor(){
        return (float)(Math.min(Math.abs(rng.nextGaussian()),1.0f)); //Mostly smaller asteroids.
        //tbh I don't even want a folded-over Gaussian distribution but it's the easiest way to go lol
    }

    private void updateSectorScale(){
        sectorScale = (int) ServerConfig.SECTOR_SIZE.getCurrentState();
    }
}
